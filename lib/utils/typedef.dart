import 'package:firebase_auth/firebase_auth.dart';

typedef VoidUserCallBack = void Function(User);
typedef VoidStringCallBack = void Function(String);
